# fastq to fasta
ls *ribo_depleted.fastq.gz | xargs -I{} basename {} .fastq.gz |xargs -I{} sbatch -n1 --wrap "gunzip {}.fastq | fastq_to_fasta -Q33 -o {}.fasta"

# or maybe - ls *_hg38_UCSC_depleted.fastq | xargs -I{} basename {} .fastq | xargs -I{} fastq_to_fasta -Q33 -i {}.fastq -o {}.fasta

# dedupe!

# megahit assemble
ls *ribo_depleted.fasta | xargs -I{} basename {} .fasta | xargs -I{} sbatch -n5 --tasks-per-node=5 -p gpu --qos gpu_access --gres=gpu:4 -t 4:00:00 --mem 64000 -e {}_megahit_gpu_assembly.out --wrap "megahit --num-cpu-threads 4 --memory 60544434585 --use-gpu -r {}.fasta -o {}_megahit_gpu_assembly"

# blast to NT with taxonomy results (make sure to copy taxdb.tar.gz)
ls -d *_megahit_assembly | xargs -I{} basename {} | xargs -I{} sbatch -n 12 --tasks-per-node=12 --time=12:00:00 -e ./{}_megahit_final_contigs_blastNT.out --wrap "blastn -db ~/pine_scratch/blast_dbs/nt -num_threads 12 -max_target_seqs 1 -outfmt '6 qseqid sseqid evalue bitscore pident length sgi sacc staxids sscinames scomnames stitle' -query {}/final.contigs.fa -out {}_vs_nt.blastout"

# Do a second pass with a large contig assembler like CAP3! this thing is WONDERFUL!

# RepeatMasker to get rid of worm SINEs

./table.sh *.blastout
#can use |tee to write file from awk

# filter for human, gorilla, trog
grep -viE "homo|Human|Gorilla|troglodytes|pongo|Macaca|leucogenys|18S" *.blastout

# filter singletons?
#grep -v  multi=1.0000 ../megahit/ut/final.contigs.fa
#this doesnt work because it leaves the sequence

#make this quantatative by normalizing to contig length or something?
#re-align reads to contigs maybe?